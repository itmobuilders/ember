# The city of Ember

Prerequisites include:

* Java Software Developer's Kit (SE) 1.8 or higher
* sbt 0.13.15 or higher (we recommend 1.2.3) Note: the project includes an sbt distribution for your convenience.

To check your Java version, enter the following in a command window:
(check $JAVA_HOME, $PATH)
java -version

To check your sbt version, enter the following:
cd <Project Home>
./sbt sbtVersion

## Build and run the project

This example Play project was created from a seed template. It includes all Play components and an Akka HTTP server. 
The project is also configured with filters for Cross-Site Request Forgery (CSRF) protection and security headers.

To build and run the project:

1. Use a command window to change into the example project directory, for example: `cd ember`

2. Build the project. Enter: `./sbt run`. The project builds and starts the embedded HTTP server. Since this downloads libraries and dependencies, the amount of time required depends partly on your connection's speed.

3. After the message `Server started, ...` displays, enter the following URL in a browser: <http://localhost:9001> 
To change port go to build.sbt and change row PlayKeys.devSettings := Seq("play.server.http.port" -> "9001")

