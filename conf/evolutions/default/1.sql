# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table people (
  id                            integer auto_increment not null,
  career_objective_id           integer,
  name                          varchar(255),
  surname                       varchar(255),
  patronymic                    varchar(255),
  sex                           varchar(255),
  age                           integer,
  birthday                      timestamp,
  working_capacity              boolean default false not null,
  constraint pk_people primary key (id)
);

create table requests (
  id                            integer auto_increment not null,
  request_type_id               integer,
  vacancy_type_id               integer,
  vacancy_title                 varchar(255),
  position_count                integer not null,
  constraint pk_requests primary key (id)
);

create table request_types (
  id                            integer auto_increment not null,
  req_type                      varchar(255),
  constraint uq_request_types_req_type unique (req_type),
  constraint pk_request_types primary key (id)
);

create table accounts (
  id                            integer auto_increment not null,
  email                         varchar(255),
  password                      varchar(255),
  role                          varchar(255),
  constraint uq_accounts_email unique (email),
  constraint pk_accounts primary key (id)
);

create table vacancies (
  id                            integer auto_increment not null,
  vacancy_type_id               integer,
  title                         varchar(255),
  position_count                integer,
  constraint pk_vacancies primary key (id)
);

create table vacancy_types (
  id                            integer auto_increment not null,
  vacancy_type                  varchar(255),
  constraint uq_vacancy_types_vacancy_type unique (vacancy_type),
  constraint pk_vacancy_types primary key (id)
);

alter table people add constraint fk_people_career_objective_id foreign key (career_objective_id) references vacancy_types (id) on delete restrict on update restrict;
create index ix_people_career_objective_id on people (career_objective_id);

alter table requests add constraint fk_requests_request_type_id foreign key (request_type_id) references request_types (id) on delete restrict on update restrict;
create index ix_requests_request_type_id on requests (request_type_id);

alter table requests add constraint fk_requests_vacancy_type_id foreign key (vacancy_type_id) references vacancy_types (id) on delete restrict on update restrict;
create index ix_requests_vacancy_type_id on requests (vacancy_type_id);

alter table vacancies add constraint fk_vacancies_vacancy_type_id foreign key (vacancy_type_id) references vacancy_types (id) on delete restrict on update restrict;
create index ix_vacancies_vacancy_type_id on vacancies (vacancy_type_id);


# --- !Downs

alter table people drop constraint if exists fk_people_career_objective_id;
drop index if exists ix_people_career_objective_id;

alter table requests drop constraint if exists fk_requests_request_type_id;
drop index if exists ix_requests_request_type_id;

alter table requests drop constraint if exists fk_requests_vacancy_type_id;
drop index if exists ix_requests_vacancy_type_id;

alter table vacancies drop constraint if exists fk_vacancies_vacancy_type_id;
drop index if exists ix_vacancies_vacancy_type_id;

drop table if exists people;

drop table if exists requests;

drop table if exists request_types;

drop table if exists accounts;

drop table if exists vacancies;

drop table if exists vacancy_types;

