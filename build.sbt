
name := """Ember"""
//organization := "com.example"

PlayKeys.devSettings := Seq("play.server.http.port" -> "9001")

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.12"
crossScalaVersions := Seq("2.11.12", "2.12.4")
//crossScalaVersions := Seq("2.11.12", "2.12.8")
//scalaVersion := "2.11.12"
//crossScalaVersions := Seq("2.11.12", "2.12.4")

libraryDependencies += guice
libraryDependencies += jdbc
libraryDependencies += javaForms

// Authentication
libraryDependencies += "be.objectify" %% "deadbolt-java" % "2.6.1"
libraryDependencies += "com.feth" %% "play-authenticate" % "0.8.3"
libraryDependencies += "be.objectify" %% "deadbolt-java-gs" % "2.6.0"
libraryDependencies += "commons-io" % "commons-io" % "2.5"

// Test Database
libraryDependencies += "com.h2database" % "h2" % "1.4.196"

libraryDependencies += javaJdbc
libraryDependencies += "org.postgresql" % "postgresql" % "9.4-1206-jdbc42"
