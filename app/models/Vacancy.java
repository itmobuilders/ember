package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "vacancies")
public class Vacancy extends Model {
    public static void init(){
        create("Main Postman", 1, null);
        create("Second Postman", 1, null);
    }

    public Vacancy(){}
    public Vacancy(Integer id, VacancyType vacancyType) {
        this.id = id;
        this.vacancyType = vacancyType;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @OneToMany(mappedBy = "vacancyId")
    public Integer id;

    @ManyToOne
    public VacancyType vacancyType;
    public String title;
    public Integer positionCount;

    public static Vacancy create(String title, int type, Integer positionCount) {
        final Vacancy req = new Vacancy();

        req.vacancyType = new VacancyType();
        req.vacancyType.id  = type;
        req.title = title;
        req.positionCount = positionCount;
        req.save();
        return req;
    }

    public static Finder<Integer, Vacancy> find = new Finder<>(Vacancy.class);

//    public static List<Vacancy> findAll() {
//        return find.query().findList();
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public VacancyType getVacancyType() {
        return vacancyType;
    }

    public void setVacancyType(VacancyType vacancyType) {
        this.vacancyType = vacancyType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static Finder<Integer, Vacancy> getFind() {
        return find;
    }

    public static void setFind(Finder<Integer, Vacancy> find) {
        Vacancy.find = find;
    }
}
