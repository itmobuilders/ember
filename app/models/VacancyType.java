package models;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "vacancy_types")
public class VacancyType extends Model {
    public static void init(){
        create("postman");
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @OneToMany(mappedBy = "vacancyType")
    public Integer id;

    @Constraints.Required
    @Column(unique = true)
    public String vacancyType;

    public static Finder<Integer, VacancyType> find = new Finder<>(VacancyType.class);

    private static VacancyType findByType(final String type) {
        return find.query().where().eq("type", type).findOne();
    }

    public static VacancyType create(String type) {
        final VacancyType reqType = new VacancyType();

        if (type != null & !type.isEmpty()) {
            reqType.vacancyType = type;
            reqType.save();
        }
        return reqType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVacancyType() {
        return vacancyType;
    }

    public void setVacancyType(String vacancyType) {
        this.vacancyType = vacancyType;
    }

    public static Finder<Integer, VacancyType> getFind() {
        return find;
    }

    public static void setFind(Finder<Integer, VacancyType> find) {
        VacancyType.find = find;
    }
}
