package models;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.*;

@Entity
@Table(name = "request_types")
public class RequestType extends Model {
    public static void init(){
        create("add vacancy");
        create("add vacancy type");
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Constraints.Required
    @Column(unique = true)
    @OneToMany(mappedBy = "reqType")
    public String reqType;

    public static Finder<Integer, RequestType> find = new Finder<>(RequestType.class);

    public static RequestType create(String type) {
        final RequestType reqType = new RequestType();

        if (type != null | !type.isEmpty()) {
            reqType.reqType = type;
            reqType.save();
        }
        return reqType;
    }
}
