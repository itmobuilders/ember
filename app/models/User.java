package models;

import be.objectify.deadbolt.java.models.Permission;
import be.objectify.deadbolt.java.models.Role;
import be.objectify.deadbolt.java.models.Subject;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthUser;
import com.feth.play.module.pa.user.*;
import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints;
import providers.MyUsernamePasswordAuthUser;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "accounts")
public class User extends Model implements Subject{

    @Id
    public Integer id;

    @Constraints.Required
    @Constraints.Email
    @Column(unique = true)
    public String email;

    @Constraints.Required
    @Constraints.MinLength(5)
    public String password;

    public String role;

    public static Finder<Integer, User> find = new Finder<>(User.class);

    private static User findByEmail(final String email) {
        return find.query().where().eq("email", email).findOne();
    }

    public static User create(final AuthUser authUser) {
        final User user = new User();

        boolean success = false;
        if (authUser instanceof UsernamePasswordAuthUser) {
            final UsernamePasswordAuthUser identity = (UsernamePasswordAuthUser) authUser;
            final String email = identity.getEmail();
            final String hashedPassword = identity.getHashedPassword();
            if (email != null && hashedPassword != null) {
                user.email = email;
                user.password = hashedPassword;
                success = true;
            }
            if (authUser instanceof MyUsernamePasswordAuthUser) {
                final MyUsernamePasswordAuthUser myIdentity = (MyUsernamePasswordAuthUser) authUser;
                user.role = myIdentity.getRole();
            } else {
                user.role = "Habitant";
            }
        }
        if (!success) {
            throw new IllegalStateException("Cannot create user without email or password");
        }
        //user.roles = new ArrayList<>();

        user.save();
        return user;
    }

    public static boolean existsByAuthUserIdentity(AuthUser authUser) {
        return authUser instanceof EmailIdentity && findByEmail(((EmailIdentity) authUser).getEmail()) != null;
    }

    public static User findByAuthUserIdentity(AuthUserIdentity identity) {
        if (identity instanceof EmailIdentity) {
            return findByEmail(((EmailIdentity) identity).getEmail());
        }
        return null;
    }

    public static User findByUsernamePasswordIdentity(UsernamePasswordAuthUser user) {
        return findByEmail(user.getEmail());
    }

    @Override
    public List<? extends Role> getRoles() {
        return Arrays.asList(SecurityRole.findByRoleName(role));
    }

    @Override
    public List<? extends Permission> getPermissions() {
        return Arrays.asList(UserPermission.get());
    }

    @Override
    public String getIdentifier() {
        return email;
    }
}
