package models;

import be.objectify.deadbolt.java.models.Permission;

import javax.persistence.Entity;

/**

 */
public class UserPermission implements Permission {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public String value;

    public String getValue() {
        return value;
    }

    private UserPermission(String value) {
        this.value = value;
    }
    static final UserPermission single = new UserPermission("value");

    public static UserPermission get() {
        return single;
    }
}
