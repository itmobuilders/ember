package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "people")
public class Person extends Model {
    public static void init(){
        create("Petr", "Petrov", "Petrovich", 30, "M",
                new Date(1997,10,2), true, 1);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @ManyToOne
    public VacancyType careerObjective;
    public String name;
    public String surname;
    public String patronymic;
    public String sex;
    public Integer age;
    public Date birthday;
    public boolean workingCapacity;


    public static Person create(String name, String surname, String patronymic, Integer age,
                                String sex, Date birthday, boolean workingCapacity, int vacType) {
        final Person person = new Person();

        person.careerObjective = new VacancyType();
        person.careerObjective.id  = vacType;
        person.name = name;
        person.surname = surname;
        person.age = age;
        person.patronymic = patronymic;
        person.sex = sex;
        person.birthday = birthday;
        person.workingCapacity = workingCapacity;

        person.save();
        return person;
    }

    public static Finder<Integer, Person> find = new Finder<>(Person.class);

//    public static List<Vacancy> findAll() {
//        return find.query().findList();
//    }

    public static Finder<Integer, Person> getFind() {
        return find;
    }

    public static void setFind(Finder<Integer, Person> find) {
        Person.find = find;
    }
}
