/*
 * Copyright 2012 Steve Chaloner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package models;

import be.objectify.deadbolt.java.models.Role;

import java.util.Arrays;
import java.util.List;

/**
 * @author Steve Chaloner (steve@objectify.be)
 */
public class SecurityRole implements Role {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private static final List<String> roles = Arrays.asList("Guest", "Habitant", "Leader", "Mayor", "Postman");

	private SecurityRole(String roleName) {
		this.roleName = roleName;
	}

	public String roleName;

	@Override
	public String getName() {
		return roleName;
	}

	public static SecurityRole findByRoleName(String roleName) {
		for (String role: roles) {
			if (roleName.equals(role)) {
				return new SecurityRole(role);
			}
		}
		return new SecurityRole(roles.get(0));
	}

}
