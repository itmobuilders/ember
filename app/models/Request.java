package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;

@Entity
@SequenceGenerator(name="req_vacancy", initialValue=1000, allocationSize=100)
@Table(name = "requests")
public class Request  extends Model {
    public static void init(){
        create(1, 1, "Fifth Postman", 1);
        create(1, 1, "Sixth Postman", 1);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @ManyToOne
    public RequestType requestType;

    @ManyToOne
    public VacancyType vacancyType;
    public String vacancyTitle;
    public int positionCount;

    public static Request create(int reqType, int vacancyType, String vacancyTitle, int positionCount) {
        final Request req = new Request();
        req.requestType = new RequestType();
        req.requestType.id = reqType;
        req.vacancyType = new VacancyType();
        req.vacancyType.id = vacancyType;
        req.vacancyTitle = vacancyTitle;
        req.positionCount = positionCount;
        req.save();
        return req;
    }

    public static Finder<Integer, Request> find = new Finder<>(Request.class);
}
