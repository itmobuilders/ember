package controllers;

import be.objectify.deadbolt.java.actions.SubjectPresent;
import models.Request;
import models.RequestType;
import models.Vacancy;
import models.VacancyType;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import services.UserProvider;
import views.html.vacancies.create;

import javax.inject.Inject;

public class VacancyController extends Controller {
    private final UserProvider provider;

    @Inject
    public VacancyController( final UserProvider provider) {
        this.provider = provider;
    }

    @Inject
    FormFactory formFactory;

    @SubjectPresent
    public Result create() {
        Form<Vacancy> vacancyForm = formFactory.form(Vacancy.class);
        return ok(create.render(vacancyForm, provider));
    }

    @SubjectPresent
    public Result save() {
        Form<Vacancy> vacancyForm = formFactory.form(Vacancy.class).bindFromRequest();
        if (vacancyForm.hasErrors()) {
            return badRequest(create.render(vacancyForm, provider));
        }
        Vacancy vacancy = vacancyForm.get();
        System.out.println(vacancy.id);
        Logger.info("vacancy type: " + vacancy.vacancyType.vacancyType
                + ", vacancy id: " + vacancy.vacancyType.id);
        //vacancy.save();
        Request request = new Request();
        request.requestType = new RequestType();
        request.requestType.id = 1;
        request.vacancyType =  new VacancyType();
        request.vacancyType.id = vacancy.vacancyType.id;
        request.vacancyTitle = vacancy.title;
        request.positionCount = vacancy.positionCount;
        request.save();
        return redirect(routes.HomeController.home());
    }

//    @SubjectPresent
//    public Result index() {
//        List<Vacancy> vacancies = Vacancy.find.all();;
//
//        if (vacancies == null) {
//            return notFound("No requests");
//        }
//        return ok(index.render(vacancies));
//    }

//    public Result show(UserProvider userProvider, Integer id) {
//        Vacancy vacancy = Vacancy.find.byId(id);
//        if (vacancy == null) {
//            return notFound("User not found");
//        }
//        return ok(show.render(userProvider, vacancy));
//    }
}
