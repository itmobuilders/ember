package controllers;

import play.mvc.*;

import com.feth.play.module.pa.PlayAuthenticate;
import providers.MyUsernamePasswordAuthProvider;
import services.UserProvider;

import javax.inject.Inject;

import views.html.index;
import views.html.home;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    private final UserProvider provider;

    public static final String FLASH_MESSAGE_KEY = "message";
    public static final String FLASH_ERROR_KEY = "error";


    @Inject
    public HomeController( final UserProvider provider) {
        this.provider = provider;
    }

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */

    public Result index() {
        return ok(index.render(provider));
    }

    public Result home() {
        return ok(home.render(provider));
    }

}
