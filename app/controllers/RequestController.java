package controllers;

import be.objectify.deadbolt.java.actions.SubjectPresent;
import models.Request;
import models.RequestType;
import models.Vacancy;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import services.UserProvider;

import javax.inject.Inject;
import java.util.List;

import services.VacancyProvider;
import views.html.vacancies.create;
import views.html.requests.details;
//import views.html.requests_module;

public class RequestController extends Controller {
    private final UserProvider provider;

    @Inject
    public RequestController( final UserProvider provider) {
        this.provider = provider;
    }

    @Inject
    FormFactory formFactory;

//    @SubjectPresent
//    public Result save() {
//        Form<Vacancy> vacancyForm = formFactory.form(Vacancy.class).bindFromRequest();
//        if (vacancyForm.hasErrors()) {
//            return badRequest(create.render(vacancyForm, provider));
//        }
//        Vacancy vacancy = vacancyForm.get();
//        Request request = new Request();
//        request.requestType = new RequestType();
//        request.requestType.id = 1;
//        request.vacancyType = vacancy.vacancyType;
//        request.vacancyTitle = vacancy.title;
//        request.save();
//        return redirect(routes.HomeController.home());
//    }
//    @SubjectPresent
//    public Result show() {
//        List<Request> requests = Request.find.all();
//        if (requests == null) {
//            return notFound("No requests");
//        }
//        //return ok(show.render(requests));
//        return null;
//    }
    @SubjectPresent
    public Result processing(Integer id){
        Request request = Request.find.byId(id);
        if(request == null){
            return notFound("Request not found");
        }
        Vacancy vacancy = new Vacancy();
        vacancy.vacancyType = request.vacancyType;
        vacancy.title = request.vacancyTitle;
        vacancy.save();

        request.delete();
        return redirect(routes.HomeController.home());
    }

    @SubjectPresent
    public Result details(Integer id) {
        Request request = Request.find.byId(id);
        if (request == null) {
            return notFound("No requests");
        }
        //return ok(show.render(requests));
        return ok(details.render(request, provider));
    }

    @SubjectPresent
    public Result destroy(Integer id) {
        Request request = Request.find.byId(id);
        if (request == null) {
            return notFound("No requests");
        }
        request.delete();
        //return ok(show.render(requests));
        return redirect(routes.HomeController.home());
    }
}
