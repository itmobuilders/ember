package controllers;

import be.objectify.deadbolt.java.actions.SubjectPresent;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.ebean.DuplicateKeyException;
import models.User;
import modules.HashHelper;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.UserProvider;
import views.html.users.*;

import javax.inject.Inject;
import java.util.List;


public class UserController extends Controller {

    @Inject
    FormFactory formFactory;
    //

    private final UserProvider provider;

    @Inject
    public UserController(final UserProvider provider) {
        this.provider = provider;
    }


    public Result index() {
        List<User> users = User.find.all();
        return ok(index.render(users));
    }
    @SubjectPresent
    public Result create() {
        Form<User> userForm = formFactory.form(User.class);
        return ok(create.render(userForm, ""));
    }
    @SubjectPresent
    public Result save() {
        Form<User> userForm = formFactory.form(User.class).bindFromRequest();
		if (userForm.hasErrors()) {
			return badRequest(create.render(userForm, "Invalid field"));
		}
        User user = userForm.get();
        user.password = HashHelper.createPassword(user.password);
        try {
			user.save();
		}
		catch (DuplicateKeyException duplicateException) {
			return badRequest(create.render(userForm, "User " + user.email + " already exists"));
		}
		catch (Exception e) {
            return badRequest(create.render(userForm, "Internal error: " + e.getMessage()));
        }
        return redirect(routes.UserController.index());
    }
    @SubjectPresent
    public Result edit(Integer id) {
        User user = User.find.byId(id);
        if (user == null) {
            return notFound("User not found");
        }
        Form<User> userForm = formFactory.form(User.class).fill(user);

        return ok(edit.render(userForm));
    }
    @SubjectPresent
    public Result update() {
        Form<User> userForm = formFactory.form(User.class).bindFromRequest();
		if (userForm.hasErrors()) {
			return badRequest(edit.render(userForm));
		}
        User user = userForm.get();
        User oldUser = User.find.byId(user.id);
        if (oldUser == null) {
            return notFound("User not found");
        }
        oldUser.email = user.email;
        oldUser.password = HashHelper.createPassword(user.password);
        oldUser.update();
        return redirect(routes.UserController.index());
    }
    @SubjectPresent
    public Result destroy(Integer id) {

        User user = User.find.byId(id);
        if (user == null) {
            return notFound("User not found");
        }

        user.delete();

        return redirect(routes.UserController.index());
    }

    public Result show(Integer id) {
        User user = User.find.byId(id);
        if (user == null) {
            return notFound("User not found");
        }
        return ok(show.render(user));
    }

}
