package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import play.routing.JavaScriptReverseRouter;

public class RouteController extends Controller {
    public Result untrail(String path) {
        return movedPermanently("/" + path);
    }

    /*public Result javascriptRoutes() {
        return ok(
                JavaScriptReverseRouter.create("jsRoutes",
                        routes.javascript.PlaylistController.destroy(),
                        routes.javascript.PlaylistController.save(),
                        routes.javascript.PlaylistController.removeMusic(),
                        routes.javascript.PlaylistController.getMusic(),
                        routes.javascript.UserController.getPlaylists(),
                        routes.javascript.UserController.getMusic(),
                        routes.javascript.MusicController.destroy(),
                        routes.javascript.MusicController.download(),
                        routes.javascript.MusicController.upload(),
                        routes.javascript.MusicController.update(),
                        routes.javascript.MusicController.searchMusicByName(),
                        routes.javascript.MusicController.searchMusicByTag(),
                        routes.javascript.PlaylistController.addMusic()
                )
        ).as("text/javascript");
    }*/
}