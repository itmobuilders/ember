package controllers;

import be.objectify.deadbolt.java.actions.SubjectNotPresent;
import com.feth.play.module.pa.PlayAuthenticate;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import services.UserProvider;
import views.html.index;
import views.html.login;
import views.html.signup;

import javax.inject.Inject;

public class LoginController extends Controller {
    private final PlayAuthenticate auth;

    private final MyUsernamePasswordAuthProvider provider;

    private final UserProvider userProvider;

    @Inject
    public LoginController(final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
                           final UserProvider userProvider) {
        this.auth = auth;
        this.provider = provider;
        this.userProvider = userProvider;
    }

    @SubjectNotPresent
    public Result login() {
        return ok(login.render(this.auth, this.userProvider,  this.provider.getLoginForm()));
    }

    @SubjectNotPresent
    public Result doLogin() {
        com.feth.play.module.pa.controllers.Authenticate.noCache(response());
        final Form<MyUsernamePasswordAuthProvider.MyLogin> filledForm = this.provider.getLoginForm()
                .bindFromRequest();
        if (filledForm.hasErrors()) {
            // User did not fill everything properly
            return badRequest(login.render(this.auth, this.userProvider, filledForm));
        } else {
            // Everything was filled
            return this.provider.handleLogin(ctx());
        }
    }
    @SubjectNotPresent
    public Result signup() {
        return ok(signup.render(this.auth, this.userProvider, this.provider.getSignupForm()));
    }
    @SubjectNotPresent
    public Result doSignup() {
        com.feth.play.module.pa.controllers.Authenticate.noCache(response());
        final Form<MyUsernamePasswordAuthProvider.MySignup> filledForm = this.provider.getSignupForm().bindFromRequest();
        if (filledForm.hasErrors()) {
            // User did not fill everything properly
            return badRequest(signup.render(this.auth, this.userProvider, filledForm));
        } else {
            // Everything was filled
            // do something with your part of the form before handling the user
            // signup
            return this.provider.handleSignup(ctx());
        }
    }

    public Result oAuthDenied(final String providerKey) {
        System.out.println("denied");
        return ok(index.render(userProvider));
    }

}
