package services;

import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;
import models.*;
import play.mvc.Http.Session;

import javax.inject.Inject;

/**
 * Service layer for User DB entity
 */
public class UserProvider {

    private final PlayAuthenticate auth;

    @Inject
    public UserProvider(final PlayAuthenticate auth) {
        if (VacancyProvider.getVacancyTypes() == null
                | VacancyProvider.getVacancyTypes().isEmpty()) {
            VacancyType.init();
            Vacancy.init();
            Person.init();
            RequestType.init();
            Request.init();
        }
        this.auth = auth;
    }

    public User getUser(Session session) {
        final AuthUser currentAuthUser = this.auth.getUser(session);
        final User localUser = User.findByAuthUserIdentity(currentAuthUser);
        return localUser;
    }
}
