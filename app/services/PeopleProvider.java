package services;

import models.Person;
import models.Vacancy;
import models.VacancyType;

import java.util.List;

public class PeopleProvider {
    public static List<Person> getPeople(){
        final List<Person> people = Person.find.all();
        return people;
    }
}
