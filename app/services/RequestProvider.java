package services;

import models.Request;
import models.RequestType;

import java.util.List;

public class RequestProvider {
    public static List<Request> getRequests(){
        final List<Request> requests = Request.find.all();
        return requests;
    }

    public static RequestType getRequestType(int id){
        final RequestType requestType = RequestType.find.byId(id);
        return requestType;
    }
}
