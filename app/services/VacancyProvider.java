package services;

import models.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VacancyProvider {
    private static void init(List<Vacancy> vacancies){
        while (vacancies.isEmpty()) {
            vacancies = Vacancy.find.all();
        }
    }
    public static List<Vacancy> getVacancies(){
        final List<Vacancy> vacancies = Vacancy.find.all();
        init(vacancies);
        return vacancies;
    }

    public static List<VacancyType> getVacancyTypes(){
        final List<VacancyType> vacancyTypes = VacancyType.find.all();
        return vacancyTypes;
    }

    public static Map<String, String> getVacancyTypesStr(){
        List<VacancyType> vacancyTypes = getVacancyTypes();
        Map<String, String> vacancyTypesStr = new HashMap<>();

        for (VacancyType type:vacancyTypes){
            vacancyTypesStr.put(type.getId().toString(), type.getVacancyType());
        }
        return vacancyTypesStr;
    }

    public static Vacancy getVacancy(int id){
        final Vacancy vacancy = Vacancy.find.byId(id);
        return vacancy;
    }

    public static VacancyType getVacancyType(int id){
        final VacancyType vacancyType = VacancyType.find.byId(id);
        return vacancyType;
    }
}
